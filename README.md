<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Perpustakaan
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-perpustakaan.git`
2. Go inside the folder: `cd laravel-perpustakaan`
3. Run `cp .env.example .env` then put your database name & credentials.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate --seed`
7. Run `php artisan serve`
9. Login username: admin --> uzumaki_naruto@konohagakure.co.jp/naruto2020 | admin --> uchiha_sasuke@konohagakure.co.jp/naruto2020
8. Open your favorite browser: http://localhost:8000

### Screen shot

Registration Page

![Registration Page](img/register.png "Registration Page")

Login Page

![Login Page](img/login.png "Login Page")

Home Page

![Home Page](img/home.png "Home Page")

Anggota Page

![Anggota Page](img/anggota.png "Anggota Page")

Buku Page

![Buku Page](img/buku.png "Buku Page")

User Page

![User Page](img/user.png "User Page")

Laporan Buku Page

![Laporan Buku Page](img/laporan_buku.png "Laporan Buku Page")

Transaksi Page

![Transaksi Page](img/transaksi.png "Transaksi Page")

![Transaksi Page](img/transaksi2.png "Transaksi Page")

![Buku Page](img/buku.png "Buku Page")



