<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'id' => 1,
                'name' => 'Gilacoding - Admin',
                'username' => 'admin123',
                'email' => '123456@gilacoding.com',
                'password' => bcrypt('admin123'),
                'gambar' => NULL,
                'level' => 'admin',
                'remember_token' => NULL,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 2,
                'name' => 'Gilacoding - User',
                'username' => 'user123',
                'email' => '654321@gilacoding.com',
                'password' => bcrypt('user123'),
                'gambar' => NULL,
                'level' => 'user',
                'remember_token' => NULL,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 3,
                'name' => 'Konohagakure - Admin',
                'username' => 'naruto',
                'email' => 'uzumaki_naruto@konohagakure.co.jp',
                'password' => bcrypt('naruto2020'),
                'gambar' => NULL,
                'level' => 'admin',
                'remember_token' => NULL,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 4,
                'name' => 'Konohagakure - User',
                'username' => 'sasuke',
                'email' => 'uchiha_sasuke@konohagakure.co.jp',
                'password' => bcrypt('naruto2020'),
                'gambar' => NULL,
                'level' => 'user',
                'remember_token' => NULL,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);
    }
}
