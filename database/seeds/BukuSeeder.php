<?php

use App\Buku;
use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Buku::insert([
            [
                'id' => 1,
                'judul' => 'Belajar Pemrograman Java',
                'isbn' => '9920392749',
                'pengarang' => 'Abdul Kadir',
                'penerbit' => 'PT. Restu Ibu',
                'tahun_terbit' => 2018,
                'jumlah_buku' => 20,
                'deskripsi' => 'Buku Pertama Belajar Pemrograman Java Untuk Pemula',
                'lokasi' => 'rak1',
                'cover' => 'buku_java.jpg',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 2,
                'judul' => 'Pemrograman Android',
                'isbn' => '9920395559',
                'pengarang' => 'Muhammad Nurhidayat',
                'penerbit' => 'PT. Restu Guru',
                'tahun_terbit' => 2018,
                'jumlah_buku' => 14,
                'deskripsi' => 'Jurus Rahasia Menguasai Pemrograman Android',
                'lokasi' => 'rak2',
                'cover' => 'jurus_rahasia.jpg',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 3,
                'judul' => 'Android Application',
                'isbn' => '9920392000',
                'pengarang' => 'Hendi Santika',
                'penerbit' => 'PT. Restu Ayah',
                'tahun_terbit' => 2018,
                'jumlah_buku' => 5,
                'deskripsi' => 'Buku Pertama Belajar Pemrograman Java Untuk Pemula',
                'lokasi' => 'rak2',
                'cover' => 'create_your.jpg',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 4,
                'judul' => 'Laravel Application',
                'isbn' => '9920392000',
                'pengarang' => 'Hendi Santika',
                'penerbit' => 'PT. Restu Ayah Ibu',
                'tahun_terbit' => 2018,
                'jumlah_buku' => 5,
                'deskripsi' => 'Buku Pertama Belajar Pemrograman Laravel Untuk Pemula',
                'lokasi' => 'rak2',
                'cover' => 'create_your.jpg',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ]);
    }
}
